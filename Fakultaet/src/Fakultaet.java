import java.util.Scanner;

public class Fakultaet {

	public static void main(String[] args) 
	{
		System.out.println(fak(3));
	}
	
	public static int fak(int zahl)
	{
		int erg = 1;
		int n = 1;
		
		if(zahl > 0)
		{
			if(zahl <= 20)
			{
				while(n <= zahl) 
				{
					erg = erg * n;
					n++;
				}
				System.out.println(zahl + "! = " + erg);
			}
			else
			{
				System.out.println("Geben Sie bitte eine Zahl zwischen 0 und 20 ein!");
			}
		}
		else
		{
			if(zahl == 0)
			{
				System.out.println("0! = 1");
			}
			else
			{
				System.out.println("Geben Sie bitte eine Zahl zwischen 0 und 20 ein!");
			}
		}
		return erg;
	}
}
