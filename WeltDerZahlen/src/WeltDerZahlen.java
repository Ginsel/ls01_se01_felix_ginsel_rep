/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie duerfen nicht die Namen der Variablen veraendern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Felix W. Ginsel >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
	  int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstrasse
      long anzahlSterne = 200000000000l;
    
    // Wie viele Einwohner hat Berlin?
      float bewohnerBerlin = 3.7f;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       int alterTage = 8079;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm = 200000;
    
    // Schreiben Sie auf, wie viele km� das groesste Land der Erde hat?
       int flaecheGroessteLand = 17098242;
    
    // Wie groesste ist das kleinste Land der Erde?
    
       int flaecheKleinsteLand = 2;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne);

    System.out.println("Anzahl der Einwohner von Berlin: " + bewohnerBerlin+"Mio.");
    
    System.out.println("Mein ALter in Tagen: " + alterTage + " Tage");
    
    System.out.println("Gewicht des schwersten Tiers der Welt: " + gewichtKilogramm+"kg");
    
    System.out.println("Fl�che des gr��ten Landes der Welt: " + flaecheGroessteLand+"km�");
    
    System.out.println("Fl�che des kleinsten Landes der Welt: " + flaecheKleinsteLand+"km�");
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

