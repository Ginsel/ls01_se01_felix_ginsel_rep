
public class Textverarbeitung {

	public static void main(String[] args) {
		System.out.printf("%d\n", indexDesZeichens("Hello World", 'o'));

	}
	
	static int indexDesZeichens(String s, char c) {
		int index = -1;
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == c) {
				index = i;
			}
		}
		return index;
	}
}
