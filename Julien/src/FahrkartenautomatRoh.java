﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen("Geben Sie die Anzahl der gwünschten Tickets ein: ", 2.90);
    	double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
    	
    }
    
    
    public static double fahrkartenbestellungErfassen(String text, double preis) 
    {
    	
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println(text);
    	
    	int anzahl = myScanner.nextInt();
    	
    	double zuZahlenderBetrag = anzahl * preis;
    	
    	return zuZahlenderBetrag;
    }

    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
 	
 	   Scanner myScanner = new Scanner(System.in);
 	   
 	   double eingezahlterGesamtbetrag = 0.0;
 	   double eingeworfeneMünze;
 	   
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s %.2f %s %n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = myScanner.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }

        return eingezahlterGesamtbetrag;
    }
    
    
    public static void fahrkartenAusgeben() {
        
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(125);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    
    public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	
    	double rückgabebetrag; 
    	
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
     	   //System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.00;
            }
            while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.00;
            }
            while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
        
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");       
     }

}