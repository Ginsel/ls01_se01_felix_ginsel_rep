﻿import java.util.Scanner;

class Fahrkartenautomat1
{
    public static void main(String[] args){
    	
    	//double ab = 2.90;
    	//double bc = 3.30;
    	//double abc = 3.60; 
    	
    	//double abfrage = welcherBereich("Welcher Tarifbereich(Eingabe: AB/BC/ABC): ", ab, bc, abc);
    	
    	double gesamt = fahrkartenbestellungErfassen("Wieviele Fahrkarten wollen Sie?", 2.90);     
    	double eingezahlt = fahrkartenBezahlen(gesamt);      
    	fahrkartenAusgeben();    
    	double berechnungRueck = rueckgeldAusgeben(gesamt, eingezahlt);
    }
    
   /* public static double welcherBereich(String text, double ab, double bc, double abc){
    	
    	double placeholder = 0.0;
    	
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println(text);
    	String eingabe = myScanner.next();
    	
    	if(eingabe.equals("AB")) {
    		
    		double tarif = ab;
    		System.out.println("Sie haben Tarifbereich AB gewählt.");
    		
        	return tarif;
    	}else {
    		if(eingabe.equals("BC")) {
        		
        		double tarif = bc;
        		System.out.println("Sie haben Tarifbereich BC gewählt.");
        		
            	return tarif;
    		}else {
    			if(eingabe.equals("ABC")) {
    				
    				double tarif = abc;
    				System.out.println("Sie haben Tarifbereich ABC gewählt.");
    				
    				return tarif;
    			}else {
    				return placeholder;
    			}
    		}
    	}
    			
    } */
    
    public static double fahrkartenbestellungErfassen(String text, double preis){
    	
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int eingabe = myScanner.nextInt();
		
    	double fahrkartenPreis = eingabe * preis;
    	
    	return fahrkartenPreis;
    }
      
    
    public static double fahrkartenBezahlen(double fahrkartenPreis) {
    	
    	Scanner myScanner = new Scanner(System.in);
    	double eingezahlterBetrag = 0.0;
    	double eingeworfeneMuenze;
    	
    	while(eingezahlterBetrag < fahrkartenPreis) 
    	{
    		System.out.printf("%s %.2f %s %n", "Noch zu zahlen: ", (fahrkartenPreis - eingezahlterBetrag), "€");
    		System.out.print("Einwurf(aktzeptiert: 5ct, 10ct, 20ct, 50ct, 1EUR, 2EUR): ");
    		eingeworfeneMuenze = myScanner.nextDouble();
    		eingezahlterBetrag += eingeworfeneMuenze;
    	}
    	
    	return eingezahlterBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
    	
    	 System.out.println("\nFahrschein wird ausgegeben...");
         for (int i = 0; i < 12; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\nIhre Fahrscheine wurden ausgegeben. \n \n");
    	
    }
    
    public static double rueckgeldAusgeben(double zuZahlen, double gezahlt) {
    
    	double rueckgabebetrag = gezahlt - zuZahlen;
    	
    	if(rueckgabebetrag > 0.0)
    	{
    		System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von", rueckgabebetrag, "€");
    		System.out.println("wird in folgenden Münzen ausgezahlt:");
 	   	
    		while(rueckgabebetrag >= 2.00) // 2 EURO-Münzen
    		{
    			System.out.println("2 EURO");
    			rueckgabebetrag -= 2.00;
    		}
    		while(rueckgabebetrag >= 1.00) // 1 EURO-Münzen
    		{
    			System.out.println("1 EURO");
    			rueckgabebetrag -= 1.00;
    		}
    		while(rueckgabebetrag >= 0.50) // 50 CENT-Münzen
    		{
    			System.out.println("50 CENT");
    			rueckgabebetrag -= 0.50;
    		}
    		while(rueckgabebetrag >= 0.20) // 20 CENT-Münzen
    		{
    			System.out.println("20 CENT");
    			rueckgabebetrag -= 0.20;
    		}
    		while(rueckgabebetrag >= 0.10) // 10 CENT-Münzen
    		{
    			System.out.println("10 CENT");
    			rueckgabebetrag -= 0.10;
    		}	
    		while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
    		{
    			System.out.println("5 CENT");
    			rueckgabebetrag -= 0.05;
    		}
 	   	
    	}

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
        
    	return rueckgabebetrag;
    } 
    
}