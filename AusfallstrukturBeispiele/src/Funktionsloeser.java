import java.util.Scanner;

public class Funktionsloeser {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		double e = 2.718;
		System.out.println("Geben Sie einen Wert x ein.");
		double x = input.nextDouble();
		
		funktionswert(e, x);
	}
	
	public static void funktionswert(double e, double x)
	{
		if (x <= 0) 
		{
			double f = Math.pow(e, x);
			System.out.printf("Der exponentielle Funktionswert betr�gt: %+.2f%n", f);
		}
		else
		{
			if(x > 0 && x <= 3)
			{
				double f = (x * x) + 1;
				System.out.printf("Der quadratische Funktionswert betr�gt: %+.2f%n", f);
			}
			else
			{
				double f = (2 * x) + 4;
				System.out.printf("Der lineare Funktionswert betr�gt: h                                                                                                                                                                                                                                                                                                                                                                                                                                %+.2f%n", f);
			}
		}
	}
}
