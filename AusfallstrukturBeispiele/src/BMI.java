import java.util.Scanner;

public class BMI {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte Ihre Gr��e cm ein: ");
		double gr��e = input.nextDouble();
		
		System.out.println("Geben Sie bitte Ihr Gewicht in kg ein: ");
		double gewicht = input.nextDouble();
		
		System.out.println("Ist Ihr Geschlecht m�nnlich [m] oder weiblich [w]?");
		char geschlecht = input.next().charAt(0);
		
		double bmi = bmiBerechnen(gr��e, gewicht);
		bmiAusgeben(bmi, geschlecht);
	}
	
	public static double bmiBerechnen(double gr, double gew) 
	{
		double bmi = gew / ((gr/100)*(gr/100));
		return bmi;
	}
	
	public static void bmiAusgeben(double bmi, char geschlecht) 
	{
		if(geschlecht == 'm' || geschlecht == 'f') 
		{
			if(geschlecht == 'm') 
			{
				if(bmi < 20) 
				{
					System.out.printf("Ihr BMI betr�gt: %.0f%s%n", bmi, "; Somit sind Sie Untergewichtig.");
				}
				else 
				{
					if(bmi >= 20 && bmi <= 25) 
					{
						System.out.printf("Ihr BMI betr�gt: %.0f%s%n", bmi, "; Somit sind Sie Normalgewichtig.");	
					}
					else 
					{
						System.out.printf("Ihr BMI betr�gt: %.0f%s%n", bmi, "; Somit sind Sie �bergewichtig.");
					}
				}
			}
			else
			{
				if(bmi < 19)
				{
					System.out.printf("Ihr BMI betr�gt: %.0f%s%n", bmi, "; Somit sind Sie Untergewichtig.");
				}
				else 
				{
					if(bmi >= 19 && bmi <= 24)
					{
						System.out.printf("Ihr BMI betr�gt: %.0f%s%n", bmi, "; Somit sind Sie Normalgewichtig.");	
					}
					else
					{
						System.out.printf("Ihr BMI betr�gt: %.0f%s%n", bmi, "; Somit sind Sie �bergewichtig.");
					}
				}
			}
		}
		else 
		{
			System.out.println("Geben Sie ein g�ltiges Geschlecht ([m] oder [f]) ein!");
		}
	}
}
