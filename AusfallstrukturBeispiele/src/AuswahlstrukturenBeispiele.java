import java.util.Scanner;

public class AuswahlstrukturenBeispiele {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Geben Sie bitte die erste Zahl ein:");
		int zahl1 = input.nextInt();
		System.out.println("Geben Sie bitte die zweite Zahl ein:");
		int zahl2 = input.nextInt();
		
		gebeKleinereZahlAus(zahl1, zahl2);
	}
	
	public static void gebeKleinereZahlAus(int z1, int z2) {
		if(z1 < z2) {
			System.out.println("Die kleinere Zahl lautet: " + z1);
		}else {
			System.out.println("Die kleinere Zahl lautet: " + z2);
		}
	}
}
