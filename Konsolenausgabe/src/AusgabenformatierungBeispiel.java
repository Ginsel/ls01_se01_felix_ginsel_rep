
public class AusgabenformatierungBeispiel {
	
	public static void main(String[] args) {

/*		String - Formatierung

		System.out.printf("|%20s|%n", "123456789");//String wird bei %s eingef�gt - 20 f�r 20 Zeichen - n neue Zeile
		System.out.printf("|%-20s|%n", "123456789");//- f�r linksb�ndig
		System.out.printf("|%-20.3s|%n", "123456789");//.3 nur 3 Zeichen des Strings ausgeben
*/
/*		Dezimal - Formatierung		
		System.out.printf("|%20d|%n", 123456789);//Zahl wird bei %d eingef�gt - 20 f�r 20 Zeichen - n neue Zeile
		System.out.printf("|%-20d|%n", 123456789);//- f�r linksb�ndig
		System.out.printf("|%+-20d|%n", 123456789);//+ = Vorzeichen soll dargestellt werden 
		System.out.printf("|%+-20d|%n", -123456789);//+ = Vorzeichen soll dargestellt werden
		System.out.printf("|%020d|%n", 123456789);//0 = Leerfelder werden mit 0 gef�llt
*/
/*		Floating - Point - Formatierung
		System.out.printf("|%f|%n", 12345.6789);//%f f�r float/Kommazahlen
		System.out.printf("|%.2f|%n", 12345.6789);//.2 nur 2 Nachkommastellen
		System.out.printf("|%+20.2f|%n", 12345.6789);
*/
		System.out.printf("Der Preis des %s betr�gt %.2f %S. %n", "Monitors", 109.95, "Euro");//Aneinanderkettung m�glich
	}
}
