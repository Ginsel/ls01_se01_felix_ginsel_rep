﻿/* Operatoren.java
   Uebung zu Operatoren in Java
   @author
   @version
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/
	  int x, y;  
	  System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
	  x = 75;
	  y = 23;
	  System.out.println("x = " + x );
	  System.out.println("y = " + y );
	  
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
	  int add = x + y;
	  System.out.println("x + y = " + add);
	  
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
	  int sub = x - y;
	  System.out.println("x - y = " + sub);
	  
	  int multi = x * y;
	  System.out.println("x * y = " + multi);
	  
	  int div = x / y;
	  System.out.println("x / y = " + div);
	  
	  int mod = x % y;
	  System.out.println("x % y = " + mod);
	  
	  
	  
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
	  if (x == y) {
		  System.out.println("x == y");
	  }else {
		  System.out.println("x != y");
	  }
		  
    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
	  if (x != y) {
		  System.out.println("x != y");
	  }else{
		  System.out.println("x == y");
	  }
	  
	  
	  if (x < y) {
		  System.out.println("x < y");
	  }else {
		  System.out.println("x ist größer als y");
	  }
	  
	  
	  if (x > y) {
		  System.out.println("x > y");
	  }else {
		  System.out.println("x ist kleiner als y");
	  }
	  
    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
	  if (x >= 0 && x <= 50) {
		  System.out.println("x liegt im Intervall.");
	  }else {
		  System.out.println("x liegt nicht im Intervall.");
	  }
	  
	  if (y >= 0 && y <= 50) {
		  System.out.println("y liegt im Intervall.");
	  }else {
		  System.out.println("y liegt nicht im Intervall.");
	  }
	  
  }//main
}// Operatoren
