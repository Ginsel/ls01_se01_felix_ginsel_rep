import java.util.Scanner;
//push
public class WhileSchleifeBeispiel 
{
	public static void main(String[] args) 
	{	
		String eingabe;
		
		do
		{	
			Scanner mS = new Scanner(System.in);
			
			System.out.println("Welche Methode wollen Sie ausf�hren?");
			System.out.println("hochRunter, fak, quersumme, zz oder mio?");
			
			eingabe = mS.next();
			
			if(eingabe.equals("hochRunter") || eingabe.equals("hr"))
			{
				hochRunter();
			}
			else
			{
				if(eingabe.equals("fak"))
				{
					fak();
				}
				else
				{
					if(eingabe.equals("quersumme") || eingabe.equals("qs")) 
					{
						quersumme();
					}
					else
					{
						if(eingabe.equals("zz"))
						{
							zz();
						}
						else
						{
							if(eingabe.equals("mio"))
							{
								mio();
							}
							else
							{
								System.out.printf("Geben Sie einen g�ltigen Begriff ein! %n %n");
							}
						}
					}
				}
			}
		}while(!eingabe.equals("hochRunter") && !eingabe.equals("fak") && !eingabe.equals("quersumme") && !eingabe.equals("zz") && !eingabe.equals("mio") && !eingabe.equals("qs") && !eingabe.equals("hr"));
	}
	
	public static void hochRunter()
	{
		Scanner mS = new Scanner(System.in);
		System.out.println("Wollen Sie hoch oder runter z�hlen?");
		String eingabe = mS.next();
		if(eingabe.equals("hoch"))
		{
			hoch();
		}
		else
		{
			if(eingabe.equals("runter"))
			{
				runter();
			}
			else
			{
				System.out.println("Geben Sie bitte 'hoch' oder 'runter' ein!");
			}
		}
		
	}
	
	public static void hoch() 
	{
		Scanner mS = new Scanner(System.in);
		System.out.println("Bis zu welcher Zahl soll hochgez�hlt werden?");
		int bisZahl = mS.nextInt();
		int n = 1;
		
		while(n < bisZahl) 
		{
			System.out.print(n + ", ");
			n++;
		}
		System.out.println(n);
	}
	
	public static void runter()
	{
		Scanner mS = new Scanner(System.in);
		System.out.println("Von welcher Zahl aus soll runtergez�hlt werden?");
		int bisZahl = mS.nextInt();
		int n = bisZahl;
		
		while(n > 1) 
		{
			System.out.print(n + ", ");
			n--;
		}
		System.out.println(n);
	}
	
	public static void fak()
	{
		Scanner mS = new Scanner(System.in);
		System.out.println("Von welcher Zahl soll die Fakult�t ermittelt werden?");
		int bisZahl = mS.nextInt();
		int fak = 1;
		int n = 1;
		
		if(bisZahl > 0)
		{
			if(bisZahl <= 20)
			{
				while(n <= bisZahl) 
				{
					fak = fak * n;
					n++;
				}
				System.out.println(bisZahl + "! = " + fak);
			}
			else
			{
				System.out.println("Geben Sie bitte eine Zahl zwischen 0 und 20 ein!");
			}
		}
		else
		{
			if(bisZahl == 0)
			{
				System.out.println("0! = 1");
			}
			else
			{
				System.out.println("Geben Sie bitte eine Zahl zwischen 0 und 20 ein!");
			}
		}
	}
	
	public static void quersumme()
	{
		Scanner mS = new Scanner(System.in);
		System.out.println("Von welcher Zahl soll die Quersumme ermittelt werden?");
		int bisZahl = mS.nextInt();
		int save = bisZahl;
		int qs = 0;
		int rest;
		
		if(bisZahl <= 9)
		{
			System.out.println("Quersumme von " + bisZahl + " = " + bisZahl);
		}
		else
		{
			while(0 < bisZahl) 
			{
				rest	= bisZahl % 10;
				qs		= qs + rest;
				bisZahl = bisZahl / 10;
			}
			System.out.println("Quersumme von " + save + " = " + qs);
		}
	}
	
	public static void zz()
	{
		Scanner mS = new Scanner(System.in);
		
		System.out.println("Laufzeit (in Jahren) des Sparvertrags: ");
		int jahre = mS.nextInt();
		
		System.out.println("Anzulegenes Kapital (in Euro): ");
		double kap = mS.nextDouble();
		double save = kap;
		
		System.out.println("Zinssatz: ");
		double zins = mS.nextDouble();
		zins = zins / 100;
		int n = 1;
		
		while(n <= jahre) 
		{
			kap = kap + (kap * zins);
			n++;
		}
		
		System.out.printf("Eingezahltes Kapital: %.2f EUR %n", save);
		System.out.printf("Ausgezahltes Kapital: %.2f EUR", kap);
	}
	
	public static void mio()
	{
		char auswahl;
		
		do
		{
			Scanner mS = new Scanner(System.in);
		
			int jahre = 0;
		
			System.out.println("Anzulegenes Kapital (in Euro): ");
			double kap = mS.nextDouble();
			double save = kap;
		
			System.out.println("Zinssatz: ");
			double zins = mS.nextDouble();
			zins = zins / 100;
		
			while(kap < 1000000) 
			{
				kap = kap + (kap * zins);
				jahre++;
			}
			System.out.printf("Mit diesen Konditionen sind sie nach %d Jahren Million�r! %n %n", jahre);
			System.out.println("Wollen Sie eine weitere Berechnung vornehmen?");
			System.out.println("Ja (j) oder Nein (n)?");
			auswahl = mS.next().charAt(0);
		}while(auswahl == 'j');
		
		System.out.println("Danke f�r die Nutzung des Million�rrechners!");
	}
}



































