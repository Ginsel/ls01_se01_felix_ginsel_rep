﻿
/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author		Felix W. Ginsel
    @version	1.0
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  int zaehler;
	  
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	 zaehler = 25;
	 System.out.println("Zähler: " + zaehler);
	 
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	 char eingabe;
	 
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	 eingabe = 'c';
	 System.out.println("Eingabe: " + eingabe);
	 
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	 long berechnung;
	 
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	 berechnung = 299792458l; //in m/s
	 System.out.println("Lichtgeschwindigkeit: " + berechnung + " m/s");
	 
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	 int mitglieder = 7;
	 
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	 System.out.println("Anzahl Vereinsmitglieder: " + mitglieder);
	 
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	 double ladung;
	 ladung = 1.602E-19d;
	 System.out.println("Elektrische Elementarladung: " + ladung +" As");
	 
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	 boolean zahlung;
	 
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	 zahlung = true;
	 System.out.println("Zahlung erfolgt?: " + zahlung);
	 
	 
	 //if (zahlung == true)
		 //System.out.println("Zahlung ist erfolgt!");
	 //else 
		//System.out.println("Zahlung ist nicht erfolgt!");
	 
  }//main
}// Variablen