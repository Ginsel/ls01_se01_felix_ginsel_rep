
public class AngestellterTest 
{
	public static void main(String[]args)
	{	
		//Konstruktor
		Angestellte a1 = new Angestellte();
		
		a1.setName("Max Mustermann");
		a1.setGehalt(10050.05);
		
		System.out.println("Name: " + a1.getName());
		System.out.println("Gehalt: " + a1.getGehalt());
		
		//vollparametrisierter Konstruktor
		Angestellte a2 = new Angestellte("Anna Musterfrau", 15000.00);
		
		System.out.println("Name: " + a2.getName());
		System.out.println("Gehalt: " + a2.getGehalt());
		
	}
}
