
public class Angestellte {

	private String name;
	private double gehalt;
	
	//vollparametrisierter Konstruktor
	public Angestellte(String name, double gehalt)
	{
		this.name = name;
		this.gehalt = gehalt;
	}
	
	//Konstruktor
	public Angestellte()
	{
		
	}
	
	//Setter und Getter
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setGehalt(double gehalt)
	{
		if (gehalt > 0)
		{
			this.gehalt = gehalt;
		}
	}
	
	public double getGehalt()
	{
		return this.gehalt;
	}
}
