import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		
		String artikel = liesString("Was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double netto = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");	
		double x = berechneNetto(anzahl, netto);
		double y = berechneBrutto(x, mwst);
		rechnungsAusgabe(artikel, anzahl, x, y, mwst);

	}
	public static String liesString(String text) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
	
		return artikel;
	}
	
	public static int liesInt(String text) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		
		return anzahl;
	}
	
	public static double liesDouble(String text) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();

		return preis;
	}
	
	public static double berechneNetto(double anzahl, double nettoPreis) {
		
		double nettoGesamtPreis = anzahl * nettoPreis;

		return nettoGesamtPreis;
	}
	
	public static double berechneBrutto(double nettoGesamtPreis, double mwst){
		
		double bruttoGesamtPreis = nettoGesamtPreis * (1 + mwst / 100);
		
		return bruttoGesamtPreis;
	}
	
	public static void rechnungsAusgabe(String artikel, int anzahl, double nettoGesamtPreis, double bruttoGesamtPreis, double mwst) {
		
		System.out.println("\t\t Rechnung:");
		System.out.printf("\t\t         %-20s %6s %12s %n", "Artikel", "Anzahl", "Preis(EUR)");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettoGesamtPreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttoGesamtPreis, mwst, "%");
	}
}
