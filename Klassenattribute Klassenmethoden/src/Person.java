
public class Person 
{
	private String name;
	private int alter;
	
	private static int anzahl = 0; //Klassenattribut - Objekt schreibt um; alle Objekte haben Wert
	
	public Person()
	{
		this.name = "Unbekannt";
		this.alter = 0;
		anzahl ++;
	}

	public Person(String name, int alter) 
	{
		this.name = name;
		this.alter = alter;
		anzahl ++;
	}

	public static int getAnzahl()
	{
		return anzahl;
	}
	
	public static void setAnzahl(int zahl)
	{
		anzahl = zahl;
	}
	
	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public int getAlter() 
	{
		return alter;
	}

	public void setAlter(int alter) 
	{
		this.alter = alter;
	}
	
	public String toString()
	{
		String gesamt = "Name: " + this.name + ", Alter: " + this.alter;
		return gesamt;
	}
}
