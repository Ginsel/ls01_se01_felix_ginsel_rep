
public class KundeTest {

	public static void main(String[] args)
	{
		Kunde k1 = new Kunde("Klaus", 236778942);
		
		Kunde k2 = new Kunde();
		k2.setName("Siegrid");
		k2.setKundennummer(578954621);
		
		System.out.println(k1);
		System.out.println(k2);
	}

}
