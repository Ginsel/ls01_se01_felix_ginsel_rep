
public class Kunde 
{
	private String name;
	private int kundennummer;
	
	public Kunde()
	{
		this.name = "Unbekannt";
		this.kundennummer = 0;
	}

	public Kunde(String name, int kundennummer) 
	{
		this.name = name;
		this.kundennummer = kundennummer;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public int getKundennummer() 
	{
		return kundennummer;
	}

	public void setKundennummer(int kundennummer) 
	{
		this.kundennummer = kundennummer;
	}

	public String toString() 
	{
		String gesamt = "Name: " + this.name + ", Kundennummer: " + this.kundennummer;
		return gesamt;
	}	
	
	
}
