import java.util.Scanner;

public class ArrayTasks 
{
	public static void main(String[] args) 
	{
		//beispiel();
		//a1();
		//a2();
		//a3();
		a4();
	}

	public static void beispiel()
	{
		//Array erstellen
		int [] zahlen = new int [10];
		zahlen [0] = 1;
		//Array f�llen mit Zahlen +2
		for (int i = 1; i < zahlen.length; i++)
		{
			zahlen [i] = zahlen[i - 1]  + 2;
		}
		//Array ausgeben
		System.out.print("[ ");
		for (int i = 0; i < zahlen.length; i++)
		{
			System.out.print(zahlen[i] + " ");
		}
		System.out.println("]");
	}
	
	public static void a1()
	{
		//Array erstellen
		int [] zahlen = new int [10];
		//Array f�llen
		zahlen [0] = 0;
		for (int i = 1; i < zahlen.length; i++)
		{
			zahlen [i] = zahlen[i - 1] + 1;
		}
		//Array ausgeben
		System.out.print("[ ");
		for (int i = 0; i < zahlen.length; i++)
		{
			System.out.print(zahlen[i] + " ");
		}
		System.out.println("]");
	}

	public static void a2()
	{
		//Array erstellen
		int [] zahlen = new int [10];
		zahlen [0] = 1;
		//Array f�llen mit Zahlen +2
		for (int i = 1; i < zahlen.length; i++)
		{
			zahlen [i] = zahlen[i - 1]  + 2;
		}
		//Array ausgeben
		System.out.print("[ ");
		for (int i = 0; i < zahlen.length; i++)
		{
			System.out.print(zahlen[i] + " ");
		}
		System.out.println("]");
	}

	public static void a3()
	{
		Scanner input = new Scanner(System.in);
		//Array erstellen
		char [] palin = new char [5];
		
		//Array f�llen 
		for (int i = 0; i < palin.length; i++)
		{
			palin [i] = input.next().charAt(0);
		}
		
		//Array ausgeben
		System.out.print("[ ");

		for (int i = 4; i > 0; i--)
		{
			System.out.print(palin[i] + " ");
		}
			
		System.out.println("]");
	}

	public static void a4()
	{
		boolean test = true;
		//Array erstellen und f�llen
		int [] lotto = new int [] {3, 7, 12, 18, 37, 42};
		//Array ausgeben
		System.out.print("[ ");
		for (int i = 0; i < lotto.length; i++)
		{
			System.out.print(lotto[i] + " ");
		}
		System.out.println("]");
		
		for (int i = 0; i < lotto.length; i++)
		{
			if(lotto [i] == 12)
			{
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
			}
		}
		
		for (int i = 0; i < lotto.length; i++)
		{
			if(lotto [i] == 13)
			{
				test = false;
			}
		}
		
		if (test == true)
		{
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
	}
}
