import java.util.Scanner;

public class Eingabe {//Eingabe

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl ein:");
		
		int x = myScanner.nextInt();
		
		System.out.println("Ihre Eingabe ist: " + x);
		
		
		
		//String and Char
		
		System.out.println("Geben Sie bitte Ihren Vornamen ein.");
		
		String vorname = myScanner.next();
		
		System.out.println("Vorname: " + vorname);
		
		
		
		
		
		System.out.println("Geben Sie einen Buchstaben ein.");
		
		char buchstabe = myScanner.next().charAt(0);
		
		System.out.println("Buchstabe: " + buchstabe);

	}

}
