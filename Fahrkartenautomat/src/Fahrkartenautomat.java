﻿import java.util.Scanner;
//push
class Fahrkartenautomat
{
    public static void main(String[] args){
    	
    	boolean n; 	
    	while(n = true)
    	{
    		double gesamt = fahrkartenbestellungErfassen();    
        	double eingezahlt = fahrkartenBezahlen(gesamt);      
        	fahrkartenAusgeben();    
        	rueckgeldAusgeben(gesamt, eingezahlt);
    	}
    }
    
    public static double fahrkartenbestellungErfassen()
    { 	
    	Scanner input = new Scanner(System.in);
    	int 	auswahl 	= 0;
    	double 	preis		= 0; 
    	double 	preis1 		= 0;
    	double	preis2 		= 0;
    	double 	preis3 		= 0;
    	double 	anzahl 		= 0;
    	
    	while(auswahl != 9) 
    	{     	
        	System.out.println("Bitte wählen Sie eine Karte für den Bereich Berlin AB aus.");
        	System.out.println("[1] Einzelfahrschein Regeltarif (2,90 EUR)");
        	System.out.println("[2] Tageskarte Regeltarif (8,60 EUR)");
        	System.out.println("[3] Kleingruppen - Tageskarte Regeltarif (23,50)");
        	System.out.println("[9] Bezahlen");
        	
        	auswahl = input.nextInt();
        	
        	if(auswahl == 4 || auswahl == 5 || auswahl == 6 || auswahl == 7 || auswahl == 8)
        	{
        		System.out.println("Bitte wählen sie eine gültige Eingabe.");
        	}
        	else
        	{
            	switch (auswahl)
            	{
            	case 1:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		System.out.println("Wie viele Tickets wollen Sie?");
            		anzahl = input.nextDouble();
            		preis1 = anzahl * 2.90;
            		break;
            	case 2:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		System.out.println("Wie viele Tickets wollen Sie?");
            		anzahl = input.nextDouble();
            		preis2 = anzahl * 8.60;
            		break;
            	case 3:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		System.out.println("Wie viele Tickets wollen Sie?");
            		anzahl = input.nextDouble();
            		preis3 = anzahl * 23.50;
            		break;	
            	case 9:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		break;
            	}
        	} 
        	preis = preis1 + preis2 + preis3;
        	}
    	return preis;
    }
    /*public static double fahrkartenbestellungErfassen()
    { 	
    	Scanner input = new Scanner(System.in);
    	int 	auswahl;
    	double 	preis		= 0; 
    	double 	anzahl 		= 0;
    	  	
        System.out.println("Bitte wählen Sie eine Karte für den Bereich Berlin AB aus.");
        System.out.println("[1] Einzelfahrschein Regeltarif (2,90 EUR)");
        System.out.println("[2] Tageskarte Regeltarif (8,60 EUR)");
        System.out.println("[3] Kleingruppen - Tageskarte Regeltarif (23,50)");
        System.out.println("[9] Bezahlen");
        	
        	auswahl = input.nextInt();
        	
        	if(auswahl == 4 || auswahl == 5 || auswahl == 6 || auswahl == 7 || auswahl == 8)
        	{
        		System.out.println(">>>Falsche Eingabe!<<<");
        	}
        	else
        	{
            	switch (auswahl)
            	{
            	case 1:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		System.out.println("Wie viele Tickets wollen Sie?");
            		anzahl = input.nextDouble();
            		preis = anzahl * 2.90;
            		break;
            	case 2:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		System.out.println("Wie viele Tickets wollen Sie?");
            		anzahl = input.nextDouble();
            		preis = anzahl * 8.60;
            		break;
            	case 3:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		System.out.println("Wie viele Tickets wollen Sie?");
            		anzahl = input.nextDouble();
            		preis = anzahl * 23.50;
            		break;	
            	case 9:
            		System.out.printf("Ihre Wahl: %d %n%n", auswahl);
            		break;
            	}
        	}
    	return preis;
    }*/
    
    public static double fahrkartenBezahlen(double fahrkartenPreis) {
    	
    	Scanner myScanner = new Scanner(System.in);

    	double eingezahlterBetrag = 0.0;
    	double eingeworfeneMuenze;
    	
    	while(eingezahlterBetrag < fahrkartenPreis) 
    	{
    		System.out.printf("%s %.2f %s %n", "Noch zu zahlen: ", (fahrkartenPreis - eingezahlterBetrag), "€");
    		System.out.print("Einwurf(min 5ct, max 10EUR): ");
    		eingeworfeneMuenze = myScanner.nextDouble();
    		eingezahlterBetrag += eingeworfeneMuenze;
    	}
    	
    	return eingezahlterBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
    	 System.out.println("\nFahrscheine werden ausgegeben...");
         for (int i = 0; i < 12; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(125);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\nIhre Fahrscheine wurden ausgegeben. \n \n");
    }
    
    public static void rueckgeldAusgeben(double zuZahlen, double gezahlt) {
    
    	double rueckgabebetrag = gezahlt - zuZahlen;
    	
    	if(rueckgabebetrag > 0.0)
    	{
    		System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von", rueckgabebetrag, "€");
    		System.out.println("wird in folgenden Münzen ausgezahlt:");
 	   	
    		while(rueckgabebetrag >= 2.00) // 2 EURO-Münzen
    		{
    			System.out.println("2 EURO");
    			rueckgabebetrag -= 2.00;
    		}
    		while(rueckgabebetrag >= 1.00) // 1 EURO-Münzen
    		{
    			System.out.println("1 EURO");
    			rueckgabebetrag -= 1.00;
    		}
    		while(rueckgabebetrag >= 0.50) // 50 CENT-Münzen
    		{
    			System.out.println("50 CENT");
    			rueckgabebetrag -= 0.50;
    		}
    		while(rueckgabebetrag >= 0.20) // 20 CENT-Münzen
    		{
    			System.out.println("20 CENT");
    			rueckgabebetrag -= 0.20;
    		}
    		while(rueckgabebetrag >= 0.10) // 10 CENT-Münzen
    		{
    			System.out.println("10 CENT");
    			rueckgabebetrag -= 0.10;
    		}	
    		while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
    		{
    			System.out.println("5 CENT");
    			rueckgabebetrag -= 0.05;
    		}
 	   	
    	}

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n");
    } 
    
}